class DZR_PaperWritten extends Paper
{
	void SetPaperWrittenText(WrittenNoteData data)
	{
		m_NoteContents = data;
	}

    override void SetActions()
	{
		super.SetActions();
		AddAction(ActionWritePaper);
		AddAction(ActionReadPaper);
	}
}