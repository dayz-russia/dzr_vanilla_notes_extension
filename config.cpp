class CfgPatches
{
	class dzr_vanilla_notes_extension
	{
		requiredAddons[] = {"DZ_Data","DZ_Gear_Consumables"};
		units[] = {};
		weapons[] = {};
	};
};
class CfgMods
{
	class dzr_vanilla_notes_extension
	{
		type = "mod";
		author = "DayZRussia";
		description = "Upgraded vanilla notes.";
		dir = "dzr_vanilla_notes_extension";
		name = "DZR Vanilla Notes Extension";
		dependencies[] = {"Game","World","Mission"};
		class defs
		{
			class gameScriptModule
			{
				files[] = {"dzr_vanilla_notes_extension/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_vanilla_notes_extension/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_vanilla_notes_extension/5_Mission"};
			};
		};
	};
};
class CfgVehicles
{
	
	class Inventory_Base;
	class Paper: Inventory_Base
	{
		model = "\dzr_vanilla_notes_extension\dzr_paper_blank.p3d";
	};
	
	class DZR_PaperWritten: Paper
	{
		canBeSplit=0;
        varStackMax=1;
		scope = 2;
		displayName = "$STR_DZR_Paper_Written";
		descriptionShort = "$STR_DZR_Paper_Written_DESC";
		model = "\dzr_vanilla_notes_extension\dzr_paper_written.p3d";
		inventorySlot[] = {"Paper","PaperWritten"};
	};

};
